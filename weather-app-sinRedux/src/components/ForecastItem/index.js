import React from 'react';
import PropTypes from 'prop-types';
import WeatherData from './../WeatherLocation/WeatherData';

const ForecasItem = ({weekDay, hour, data}) => (

    <div>
        <h3>{weekDay} - {hour}h </h3>
        <WeatherData data={data}></WeatherData>
    </div>
);


ForecasItem.propTypes = {

    weekDay: PropTypes.string.isRequired,
    hour:  PropTypes.number.isRequired,
    data: PropTypes.shape({

        temperature: PropTypes.number.isRequired,
        weatherState: PropTypes.string.isRequired,
        humidity: PropTypes.number.isRequired,
        wind: PropTypes.string.isRequired,
    }),
}

export default ForecasItem;