import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ForecastItem from './ForecastItem';
import './style.css';
import {CircularProgress} from '@material-ui/core';
import transformForecast from './../services/transformForecast';

const APIKEY = "f99bbd9e4959b513e9bd0d7f7356b38d";
const URL_weather = "http://api.openweathermap.org/data/2.5/forecast";

class ForecastExtended extends Component{

    constructor(){

        super();
        this.state = {
            forecastData: null,
        }
    }

    componentDidMount(){ // solo se utiliza una vez 

        this.updateCity(this.props.city);
    }

    componentWillReceiveProps(nextProps){ // se ejecuta siempre que se modifican las propiedades, exceptuando la primera vez que se establece el componente, por eso es necesario componentDidMount 

        if(nextProps.city !== this.props.city){
            this.setState({forecastData: null});
            this.updateCity(nextProps.city);
        }
    }

    updateCity = city => {

        const url_forecast = `${URL_weather}?q=${this.props.city}&appid=${APIKEY}`;
        fetch(url_forecast).then(
            data => (data.json())
        ).then(
            Weather_data => {
                const forecastData = transformForecast(Weather_data);
                this.setState({forecastData: forecastData})
            }
        )
    }

    renderForecasItemDays(forecastData){
        return forecastData.map(forecast => (
            <ForecastItem 
                key= {`${forecast.weekDay}${forecast.hour}`}
                weekDay={forecast.weekDay} 
                hour={forecast.hour} 
                data={forecast.data}>
            </ForecastItem>
        ));
    }

    renderProgress (){

        return <h3>Cargando Pronóstico extendido..</h3>;
    }

    render() {
        const { city } = this.props;
        const {forecastData} = this.state;
        return(
            <div>
                <h2 className="forecast-title">Pronóstico Extendido para {city}</h2>
                {forecastData ?
                    this.renderForecasItemDays(forecastData) :
                    <CircularProgress />
                }
            </div>);
    }
}

ForecastExtended.propTypes = {

    city: PropTypes.string.isRequired,
}

export default ForecastExtended;

