import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

const WeatherExtraInfo = ({humidity, wind}) => (

    <div className="weatherExtraInfo_container">
        <span className="extraInfo_text">{`Humedad: ${humidity} % `}</span>
        <span className="extraInfo_text">{`Vientos: ${wind} `}</span>
    </div>  
);

WeatherExtraInfo.propTypes = {

    humidity: PropTypes.number.isRequired,
    wind: PropTypes.string.isRequired,
}

export default WeatherExtraInfo;