
const APIKEY = "f99bbd9e4959b513e9bd0d7f7356b38d";
const URL_weather = "http://api.openweathermap.org/data/2.5/weather";

const getUrlByCity = city =>{

    return `${URL_weather}?q=${city}&appid=${APIKEY}`;

}

export default getUrlByCity;